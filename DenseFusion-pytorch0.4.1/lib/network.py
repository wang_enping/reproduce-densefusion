import argparse
import os
import random
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision.transforms as transforms
import torchvision.utils as vutils
from torch.autograd import Variable
from PIL import Image
import numpy as np
import pdb
import torch.nn.functional as F
from lib.pspnet import PSPNet

psp_models = {
    'resnet18': lambda: PSPNet(sizes=(1, 2, 3, 6), psp_size=512, deep_features_size=256, backend='resnet18'),
    'resnet34': lambda: PSPNet(sizes=(1, 2, 3, 6), psp_size=512, deep_features_size=256, backend='resnet34'),
    'resnet50': lambda: PSPNet(sizes=(1, 2, 3, 6), psp_size=2048, deep_features_size=1024, backend='resnet50'),
    'resnet101': lambda: PSPNet(sizes=(1, 2, 3, 6), psp_size=2048, deep_features_size=1024, backend='resnet101'),
    'resnet152': lambda: PSPNet(sizes=(1, 2, 3, 6), psp_size=2048, deep_features_size=1024, backend='resnet152')
}

class ModifiedResnet(nn.Module):

    def __init__(self, usegpu=True):
        super(ModifiedResnet, self).__init__()

        self.model = psp_models['resnet18'.lower()]()
        self.model = nn.DataParallel(self.model)

    def forward(self, x):
        x = self.model(x)
        return x

class PoseNetFeat(nn.Module):
    def __init__(self, num_points):
        super(PoseNetFeat, self).__init__()
        self.conv1 = torch.nn.Conv1d(3, 64, 1)      # 都是1*1卷积
        self.conv2 = torch.nn.Conv1d(64, 128, 1)

        self.e_conv1 = torch.nn.Conv1d(32, 64, 1)
        self.e_conv2 = torch.nn.Conv1d(64, 128, 1)

        self.conv5 = torch.nn.Conv1d(256, 512, 1)
        self.conv6 = torch.nn.Conv1d(512, 1024, 1)

        self.ap1 = torch.nn.AvgPool1d(num_points)
        self.num_points = num_points
    def forward(self, x, emb):
        """
        x = [bs, 3, 500]
        emb = [bs, 32, 500]
        """
        x = F.relu(self.conv1(x))                  # [bs, 64, 500]  （联想nlp中embedding部分，一维卷积）
        emb = F.relu(self.e_conv1(emb))            # [bs, 64, 500]
        pointfeat_1 = torch.cat((x, emb), dim=1)   # [bs, 128, 500]

        x = F.relu(self.conv2(x))                  # [bs, 128, 500]
        emb = F.relu(self.e_conv2(emb))            # [bs, 128, 500]
        pointfeat_2 = torch.cat((x, emb), dim=1)   # [bs, 256, 500]

        x = F.relu(self.conv5(pointfeat_2))        # [bs, 512, 500]
        x = F.relu(self.conv6(x))                  # [bs, 1024, 500]

        ap_x = self.ap1(x)                         # [bs, 1024, 1]

        ap_x = ap_x.view(-1, 1024, 1).repeat(1, 1, self.num_points)  # 公共特征重复500次，# [bs, 1024, 500]
        return torch.cat([pointfeat_1, pointfeat_2, ap_x], 1) #128 + 256 + 1024

class PoseNet(nn.Module):
    def __init__(self, num_points, num_obj):
        """
        :param num_points: 输入网络的点云数目   500
        :param num_obj: 目标物体的种类   13
        """
        super(PoseNet, self).__init__()                  # 调用父类的初始化函数，既定格式
        self.num_points = num_points                     # 点云的数目
        self.cnn = ModifiedResnet()                      # 实际上就是一个PSPNet，里面用的初始特征提取是resnet18，用于提取rgb图像特征
        self.feat = PoseNetFeat(num_points)
        
        self.conv1_r = torch.nn.Conv1d(1408, 640, 1)
        self.conv1_t = torch.nn.Conv1d(1408, 640, 1)
        self.conv1_c = torch.nn.Conv1d(1408, 640, 1)

        self.conv2_r = torch.nn.Conv1d(640, 256, 1)
        self.conv2_t = torch.nn.Conv1d(640, 256, 1)
        self.conv2_c = torch.nn.Conv1d(640, 256, 1)

        self.conv3_r = torch.nn.Conv1d(256, 128, 1)
        self.conv3_t = torch.nn.Conv1d(256, 128, 1)
        self.conv3_c = torch.nn.Conv1d(256, 128, 1)

        self.conv4_r = torch.nn.Conv1d(128, num_obj*4, 1) #quaternion
        self.conv4_t = torch.nn.Conv1d(128, num_obj*3, 1) #translation
        self.conv4_c = torch.nn.Conv1d(128, num_obj*1, 1) #confidence

        self.num_obj = num_obj


    def forward(self, img, x, choose, obj):
        """
        PoseNet的前向传播，进行姿态预测，你可以看train.py部分的代码，下面之部分都与ply无关
        :param img: RGB图像的像素[bs,3,h,w]--------包含object的那部分
        :param x: cloud点云数据[bs, 500, 3]
        :param choose: 选择点云的index下标[bs, 1, 500]--------因为设定的参数都是一样的，500
        :param obj: 目标物体的序列号[bs, 1]
        :return:对图像预测的姿态
        """
        out_img = self.cnn(img)                          # 对rgb图片的特征提取，#out_img[bs, 32, h, w]，不确定32是否正确？
                                                         # h,w一定和原图片是一样的，不然之后的choose如何与点云相对应，通过上采样可得
        
        bs, di, _, _ = out_img.size()

        emb = out_img.view(bs, di, -1)                   # 进行resize [bs, 32, h * w]
        choose = choose.repeat(1, di, 1)                 # 进行复制，复制di=32次，[1, 32, 500]
        emb = torch.gather(emb, 2, choose).contiguous()  # 针对emb每个通道（di），选取500个像素，及[bs, di=32, h * w]-->[bs, di=32, 500]
                                                         # contiguous()是为了保证在GPU上连续分配

        x = x.transpose(2, 1).contiguous()               # [bs, 500, 3]-->[bs,3,500]
        ap_x = self.feat(x, emb)                         # 使用了PoseNetFeat(500)网络，dense fusion的核心部分，会把特征进行融合
                                                         # [bs, 1408 = 128 + 256 + 1024, 500]

        # 从每个pixel来预测pose parameters
        rx = F.relu(self.conv1_r(ap_x))                  # [bs, 640, 500]
        tx = F.relu(self.conv1_t(ap_x))                  # [bs, 640, 500]
        cx = F.relu(self.conv1_c(ap_x))                  # [bs, 640, 500]

        rx = F.relu(self.conv2_r(rx))                    # [bs, 256, 500]
        tx = F.relu(self.conv2_t(tx))                    # [bs, 256, 500]
        cx = F.relu(self.conv2_c(cx))                    # [bs, 256, 500]

        rx = F.relu(self.conv3_r(rx))                    # [bs, 128, 500]
        tx = F.relu(self.conv3_t(tx))                    # [bs, 128, 500]
        cx = F.relu(self.conv3_c(cx))                    # [bs, 128, 500]

        rx = self.conv4_r(rx).view(bs, self.num_obj, 4, self.num_points)   # [bs, 13, 4, 500]   四元数
        tx = self.conv4_t(tx).view(bs, self.num_obj, 3, self.num_points)   # [bs, 13, 3, 500]   平移
        cx = torch.sigmoid(self.conv4_c(cx)).view(bs, self.num_obj, 1, self.num_points)   # [bs, 13, 1, 500]   置信度
        
        b = 0                                              # 选择预测对应目标内标的矩阵参数
        out_rx = torch.index_select(rx[b], 0, obj[b])      # [bs, 4, 500]
        out_tx = torch.index_select(tx[b], 0, obj[b])      # [bs, 3, 500]
        out_cx = torch.index_select(cx[b], 0, obj[b])      # [bs, 1, 500]
        
        out_rx = out_rx.contiguous().transpose(2, 1).contiguous()  # [bs, 500, 4]
        out_cx = out_cx.contiguous().transpose(2, 1).contiguous()  # [bs, 500, 3]
        out_tx = out_tx.contiguous().transpose(2, 1).contiguous()  # [bs, 500, 1]
        
        return out_rx, out_tx, out_cx, emb.detach()         # detach()表示从图中分离出来，不做反向传播
 


class PoseRefineNetFeat(nn.Module):
    def __init__(self, num_points):
        super(PoseRefineNetFeat, self).__init__()
        self.conv1 = torch.nn.Conv1d(3, 64, 1)
        self.conv2 = torch.nn.Conv1d(64, 128, 1)

        self.e_conv1 = torch.nn.Conv1d(32, 64, 1)
        self.e_conv2 = torch.nn.Conv1d(64, 128, 1)

        self.conv5 = torch.nn.Conv1d(384, 512, 1)
        self.conv6 = torch.nn.Conv1d(512, 1024, 1)

        self.ap1 = torch.nn.AvgPool1d(num_points)
        self.num_points = num_points

    def forward(self, x, emb):
        x = F.relu(self.conv1(x))
        emb = F.relu(self.e_conv1(emb))
        pointfeat_1 = torch.cat([x, emb], dim=1)

        x = F.relu(self.conv2(x))
        emb = F.relu(self.e_conv2(emb))
        pointfeat_2 = torch.cat([x, emb], dim=1)

        pointfeat_3 = torch.cat([pointfeat_1, pointfeat_2], dim=1)

        x = F.relu(self.conv5(pointfeat_3))
        x = F.relu(self.conv6(x))

        ap_x = self.ap1(x)

        ap_x = ap_x.view(-1, 1024)
        return ap_x

class PoseRefineNet(nn.Module):
    def __init__(self, num_points, num_obj):
        """
        :param num_points: 输入网络的点云数目   500
        :param num_obj: 目标物体的种类   13
        """
        super(PoseRefineNet, self).__init__()
        self.num_points = num_points
        self.feat = PoseRefineNetFeat(num_points)
        
        self.conv1_r = torch.nn.Linear(1024, 512)
        self.conv1_t = torch.nn.Linear(1024, 512)

        self.conv2_r = torch.nn.Linear(512, 128)
        self.conv2_t = torch.nn.Linear(512, 128)

        self.conv3_r = torch.nn.Linear(128, num_obj*4) #quaternion
        self.conv3_t = torch.nn.Linear(128, num_obj*3) #translation

        self.num_obj = num_obj

    def forward(self, x, emb, obj):
        bs = x.size()[0]
        
        x = x.transpose(2, 1).contiguous()
        ap_x = self.feat(x, emb)

        rx = F.relu(self.conv1_r(ap_x))
        tx = F.relu(self.conv1_t(ap_x))   

        rx = F.relu(self.conv2_r(rx))
        tx = F.relu(self.conv2_t(tx))

        rx = self.conv3_r(rx).view(bs, self.num_obj, 4)
        tx = self.conv3_t(tx).view(bs, self.num_obj, 3)

        b = 0
        out_rx = torch.index_select(rx[b], 0, obj[b])
        out_tx = torch.index_select(tx[b], 0, obj[b])

        return out_rx, out_tx
